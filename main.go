package main

import (
	"crypto/x509"
	"fmt"
	"log"
	"os"
	"sort"
	"time"

	"gitlab.com/okunnig/certchain/bulkcheck"
	"gitlab.com/okunnig/certchain/certificate"
	"gitlab.com/okunnig/certchain/tlsclient"

	"github.com/pkg/errors"
	"gopkg.in/alecthomas/kingpin.v2"
)

func resolveAndDumpChain(cert *x509.Certificate) error {
	fmt.Fprintf(os.Stderr, "attempting to resolve certificate chain...\n")
	chains, err := certificate.ResolveChain(cert, 10*time.Second)
	if err != nil {
		return err
	}

	switch len(chains) {
	case 0:
		return errors.New("no chains found")
	case 1:
		fmt.Fprintf(os.Stderr, "found 1 chain\n")
	default:
		fmt.Fprintf(os.Stderr, "found %v chains\n", len(chains))
	}

	chainToExport := chains[0]
	chainToExport = chainToExport[:len(chainToExport)-1]
	for _, cert := range chainToExport {
		certificate.DumpInfo(os.Stderr, cert)
	}

	fmt.Fprintf(os.Stderr, "writing first chain to stdout\n")
	return certificate.WritePEMChain(os.Stdout, chainToExport)
}

func checkRemoteExpiration(address string) error {
	primaryCert, err := tlsclient.DefaultClient().GetCertFromHost(address)
	if err != nil {
		return err
	}

	fmt.Fprintf(os.Stderr, "certificate expiration:\n")
	fmt.Fprintf(os.Stderr, "  subject: %v\n", primaryCert.Subject.CommonName)
	fmt.Fprintf(os.Stderr, "     from: %v\n", primaryCert.NotBefore)
	fmt.Fprintf(os.Stderr, "    until: %v\n", primaryCert.NotAfter)
	return nil
}

func checkRemoteValidity(address string) error {
	chains, err := tlsclient.DefaultClient().ResolveVerifiedChain(address)
	if err != nil {
		return err
	}
	fmt.Fprintf(os.Stdout, "successfully fetched remote certificate chain\n")

	if len(chains) == 1 {
		fmt.Fprintf(os.Stderr, "found 1 verified chain\n")
	} else {
		fmt.Fprintf(os.Stderr, "found %v verified chains\n", len(chains))
	}

	for i, chain := range chains {
		fmt.Fprintf(os.Stderr, "dumping chain %v\n", i+1)
		for _, cert := range chain {
			certificate.DumpInfo(os.Stderr, cert)
		}
	}

	return nil
}

func must(err error) {
	if err == nil {
		return
	}

	log.Fatalf("unhandled error: %+v", err)
}

func main() {
	app := kingpin.New("certchain", "Populates your PEM-files with intermediates")

	remoteChain := app.Command("remote-chain", "Tries to build a complete chain given a remote TLS server")
	remoteChainAddr := remoteChain.Arg("address", "Remote server address (e.g. 127.0.0.1:443)").Required().String()

	chain := app.Command("chain", "Tries to build a complete chain given a PEM-certificate on disk")
	chainFilePath := chain.Arg("file-path", "Path to PEM file").Required().ExistingFile()

	check := app.Command("check", "Connect to remote server and check certificate chain")
	checkRemoteAddr := check.Arg("address", "Remote server address (e.g. 127.0.0.1:443").Required().String()

	expiry := app.Command("expiry", "Connect to remote server and show certificate expiration")
	expiryRemoteAddr := expiry.Arg("address", "Remote server address (e.g. 127.0.0.1:443").Required().String()

	bulkCheck := app.Command("check-bulk", "Check remote servers in bulk")
	bulkCheckConfigPath := bulkCheck.Arg("config-path", "Path to configuration file").Required().ExistingFile()

	switch kingpin.MustParse(app.Parse(os.Args[1:])) {
	case remoteChain.FullCommand():
		remoteCert, err := tlsclient.DefaultClient().GetCertFromHost(*remoteChainAddr)
		must(err)
		must(resolveAndDumpChain(remoteCert))
	case chain.FullCommand():
		cert, err := certificate.LoadCertFromPEM(*chainFilePath)
		must(err)
		must(resolveAndDumpChain(cert))
	case check.FullCommand():
		must(checkRemoteValidity(*checkRemoteAddr))
	case expiry.FullCommand():
		must(checkRemoteExpiration(*expiryRemoteAddr))
	case bulkCheck.FullCommand():
		config, err := bulkcheck.LoadConfig(*bulkCheckConfigPath)
		must(err)
		start := time.Now()
		results := bulkcheck.Check(*config)
		sort.Slice(results, func(i, j int) bool {
			return results[i].Host < results[j].Host
		})
		for _, result := range results {
			if result.Error != nil {
				fmt.Fprintf(os.Stdout, "%-40v => failed with error: %v\n", result.Host, result.Error)
				continue
			}

			fmt.Fprintf(os.Stdout, "%-40v => OK (expires %v)\n", result.Host, result.Cert.NotAfter)
		}
		elapsed := time.Since(start)
		fmt.Fprintf(os.Stderr, "checked %v hosts in %v ms\n", len(config.Hosts), elapsed.Milliseconds())
	}
}
