package tlsclient

import (
	"crypto/tls"
	"crypto/x509"
	"net"
	"time"

	"github.com/pkg/errors"
)

type Client struct {
	Timeout time.Duration
}

func DefaultClient() *Client {
	return &Client{Timeout: 10 * time.Second}
}

func (c *Client) dialer() *net.Dialer {
	return &net.Dialer{Timeout: c.Timeout}
}

func (c *Client) ResolveVerifiedChain(address string) ([][]*x509.Certificate, error) {
	conn, err := tls.DialWithDialer(c.dialer(), "tcp", address, &tls.Config{})
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	return conn.ConnectionState().VerifiedChains, nil
}

func (c *Client) GetCertFromHost(address string) (*x509.Certificate, error) {
	conn, err := tls.DialWithDialer(c.dialer(), "tcp", address, &tls.Config{InsecureSkipVerify: true})
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	if len(conn.ConnectionState().PeerCertificates) == 0 {
		return nil, errors.New("no certificates received from server")
	}

	return conn.ConnectionState().PeerCertificates[0], nil
}
