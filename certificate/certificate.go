package certificate

import (
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/pkg/errors"
)

func LoadCertFromPEM(path string) (*x509.Certificate, error) {
	fileContent, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	firstBlock, _ := pem.Decode(fileContent)
	if firstBlock == nil {
		return nil, errors.New("no blocks found in PEM-file")
	}

	return x509.ParseCertificate(firstBlock.Bytes)
}

func ResolveChain(cert *x509.Certificate, timeout time.Duration) ([][]*x509.Certificate, error) {
	intermediatePool := x509.NewCertPool()

	issuerURLs := cert.IssuingCertificateURL
	for {
		chain, err := cert.Verify(x509.VerifyOptions{Intermediates: intermediatePool})
		if err != nil && !errors.As(err, &x509.UnknownAuthorityError{}) {
			return nil, err
		}

		if err == nil {
			return chain, nil
		}

		if len(issuerURLs) == 0 {
			break
		}

		// get next certificate in chain
		nextURLs := make([]string, 0)
		for _, url := range issuerURLs {
			cert, err := DownloadAIACertificate(url, timeout)
			if err != nil {
				log.Printf("failed to get cert for '%v' at '%v': %+v", cert.Issuer.CommonName, url, err)
				continue
			}

			intermediatePool.AddCert(cert)
			nextURLs = append(nextURLs, cert.IssuingCertificateURL...)
		}

		issuerURLs = nextURLs
	}

	return nil, errors.New("unable to complete certificate chain")
}

func DownloadAIACertificate(url string, timeout time.Duration) (*x509.Certificate, error) {
	client := http.Client{Timeout: timeout}
	resp, err := client.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return x509.ParseCertificate(body)
}

func DumpInfo(out io.Writer, cert *x509.Certificate) {
	fmt.Fprintf(out, "subject '%v'\n", cert.Subject.CommonName)
	fmt.Fprintf(out, "   valid from: %v\n", cert.NotBefore)
	fmt.Fprintf(out, "  valid until: %v\n", cert.NotAfter)
	fmt.Fprintf(out, "        is CA: %v\n", cert.IsCA)
	fmt.Fprintf(out, "    issued by: %v\n", cert.Issuer.CommonName)
	fmt.Fprintf(out, "   issuer url: %v\n", cert.IssuingCertificateURL)
}

func WritePEMChain(out io.Writer, chain []*x509.Certificate) error {
	for _, cert := range chain {
		err := pem.Encode(out, &pem.Block{
			Type:  "CERTIFICATE",
			Bytes: cert.Raw,
		})
		if err != nil {
			return err
		}
	}

	return nil
}
