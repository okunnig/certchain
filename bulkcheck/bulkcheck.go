package bulkcheck

import (
	"crypto/x509"
	"net"
	"time"

	"gitlab.com/okunnig/certchain/tlsclient"

	"github.com/BurntSushi/toml"
	"github.com/pkg/errors"
)

type Config struct {
	Timeout Duration
	Hosts   []string
}

// Duration is a wrapper for time.Duration. The wrapper adds support for parsing
// time.Durations from toml configuration files.
type Duration struct {
	time.Duration
}

func (d *Duration) UnmarshalText(text []byte) error {
	parsedDuration, err := time.ParseDuration(string(text))
	if err != nil {
		return err
	}

	d.Duration = parsedDuration
	return nil
}

func LoadConfig(path string) (*Config, error) {
	config := Config{}
	_, err := toml.DecodeFile(path, &config)
	if err != nil {
		return nil, err
	}

	// timeout needs a sane default value
	if config.Timeout.Seconds() <= 0 {
		config.Timeout.Duration = 10 * time.Second
	}

	// check addresses are legit-ish, e.g. host:port formatted
	for _, host := range config.Hosts {
		_, _, err := net.SplitHostPort(host)
		if err != nil {
			return nil, errors.Wrap(err, "invalid config")
		}
	}

	return &config, nil
}

// CheckResult contains an error if the check failed. Address is always defined regardless.
// Cert is only set if the operation was successful.
type CheckResult struct {
	Host  string
	Error error
	Cert  *x509.Certificate
}

// Check checks all hosts listed in the given config in parallel.
func Check(config Config) []CheckResult {
	parallelism := 10
	if len(config.Hosts) < parallelism {
		parallelism = len(config.Hosts)
	}

	jobChan := make(chan string, 5)
	jobResultChan := make(chan CheckResult, 5)

	// spawn a worker pool
	for i := 0; i < parallelism; i++ {
		go func() {
			for host := range jobChan {
				client := tlsclient.Client{Timeout: config.Timeout.Duration}
				chain, err := client.ResolveVerifiedChain(host)
				if err != nil {
					jobResultChan <- CheckResult{
						Host:  host,
						Error: err,
					}
					continue
				}

				jobResultChan <- CheckResult{
					Host: host,
					Cert: chain[0][0],
				}
			}
		}()
	}

	// goroutine which feeds the worker pool with jobs
	// in order to remove buffering allocations (either channels or elsewhere)
	// we will feed the pool and retrieve results concurrently
	go func() {
		for _, host := range config.Hosts {
			jobChan <- host
		}
		close(jobChan)
	}()

	// collects the results produced by the pool
	results := make([]CheckResult, 0, len(config.Hosts))
	for i := 0; i < len(config.Hosts); i++ {
		results = append(results, <-jobResultChan)
	}
	close(jobResultChan)

	return results
}
